package com.hcl.bookstore.service;
import org.springframework.beans.factory.annotation.Autowired;

import com.hcl.bookstore.dao.LoginDao;
public class LoginService {
	@Autowired
	LoginDao dao ;

	public boolean validateUser(String username, String password) {
		return dao.validateUser(username, password);
	}

}
