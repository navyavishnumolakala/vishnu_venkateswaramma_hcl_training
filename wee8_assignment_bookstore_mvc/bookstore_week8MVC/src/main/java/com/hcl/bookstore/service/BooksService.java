package com.hcl.bookstore.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.hcl.bookstore.beans.Books;
import com.hcl.bookstore.dao.BooksDao;

public class BooksService {
	@Autowired
	BooksDao dao;

	public List<Books> getAllBooks() {
		return dao.getAllBooks();

	}

}
