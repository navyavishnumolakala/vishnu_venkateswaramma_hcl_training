package com.hcl.bookstore.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;

import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;

import com.hcl.bookstore.dao.RegisterDao;
import com.hcl.bookstore.exception.UserException;

public class RegisterService {
	@Autowired
	RegisterDao dao ;

	public boolean registerUser(String username, String password) throws DuplicateKeyException {
		return dao.registerUser(username, password);
	}

}
