package com.hcl.bookstore.main;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.hcl.bookstore.beans.Books;
import com.hcl.bookstore.beans.LikedBooks;
import com.hcl.bookstore.beans.ReadLaterBooks;
import com.hcl.bookstore.dao.BooksDao;
import com.hcl.bookstore.dao.LikedDao;
import com.hcl.bookstore.dao.LoginDao;
import com.hcl.bookstore.dao.ReadLaterDao;
import com.hcl.bookstore.dao.RecoveryDao;
import com.hcl.bookstore.dao.RegisterDao;
import com.hcl.bookstore.service.BooksService;
import com.hcl.bookstore.service.LikedBooksService;
import com.hcl.bookstore.service.LoginService;
import com.hcl.bookstore.service.ReadLaterService;
import com.hcl.bookstore.service.RecoveryService;
import com.hcl.bookstore.service.RegisterService;

@Configuration
public class MainApp {
	@Bean
	@Scope(value = "prototype")
	public Books book() {

		return new Books();
	}
	
	@Bean
	@Scope(value = "prototype")
	public LikedBooks likedBook() {

		return new LikedBooks();
	}
	
	@Bean
	@Scope(value = "prototype")
	public ReadLaterBooks readLaterBook() {

		return new ReadLaterBooks();
	}
	
	@Bean
	public RegisterService service() {

		return new RegisterService();
	}
	
	@Bean
	public LoginService service1() {

		return new LoginService();
	}
	
	@Bean
	public RecoveryService service2() {

		return new RecoveryService();
	}
	
	@Bean
	public BooksService service3() {

		return new BooksService();
	}
	
	@Bean
	public LikedBooksService service4() {

		return new LikedBooksService();
	}
	
	@Bean
	public ReadLaterService service5() {

		return new ReadLaterService();
	}
	
	@Bean
	public BooksDao dao() {

		return new BooksDao();
	}
	
	@Bean
	public LikedDao likedDao() {

		return new LikedDao();
	}
	
	@Bean
	public LoginDao loginDao() {

		return new LoginDao();
	}
	
	@Bean
	public RecoveryDao recoveryDao() {

		return new RecoveryDao();
	}
	
	@Bean
	public ReadLaterDao readLaterDao() {

		return new ReadLaterDao();
	}
	
	@Bean
	public RegisterDao registerDao() {

		return new RegisterDao();
	}
	
	
	
	
	@Bean
    public DriverManagerDataSource ds() {
    	
    	DriverManagerDataSource ds = new DriverManagerDataSource();
    	ds.setUrl("jdbc:mysql://localhost:3306/vishnu_books");
    	ds.setUsername("root");
    	ds.setPassword("Navya123@");
    	
    	return ds;
    	
    }
    @Bean
    public JdbcTemplate template() {
    	
    	JdbcTemplate template = new JdbcTemplate();
    	template.setDataSource(ds());
    	return template;
    	
    }

}
