package com.hcl.bookstore.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.hcl.bookstore.dao.RecoveryDao;

public class RecoveryService {
	@Autowired
	RecoveryDao dao;

	public boolean changePassword(String username, String password) {
		return dao.changePassword(username, password);
	}

}
