create database hai;

use hai;

create table login(username varchar(25) not null,password varchar(25) not null);

insert into login values("Arjun",1234);
insert into login values("akash",1234);
insert into login values("vishnu",1234);

select * from login;


create table registration(username varchar(25) not null, password varchar(25) not null, email varchar(30));

insert into registration values ("Navya",1234,"navya@gmail.com");
 insert into registration values ("akash",1234,"akash@gmail.com");
insert into registration values ("vinay",1234,"vinay@gmail.com");

select *from registration;


 create table books(bookid int primary key, booktitle varchar(30), bookgenre varchar(30), image varchar(30));

 insert into books values(1,"bhagavdgita","motivationl","bhagavdgita.jpg");
insert into books values(2,"tom and jerry","funny","tomandjerry.jpg");
 insert into books values(3,"life amazing secrets","motivationl","lifeamazingsecrets.jpg");
insert into books values(4,"kidsbook","funny","kidsbooks.jpg");
 insert into books values(5,"freeyourmind","motivationl","freeypurmind.jpg");
insert into books values(6,"trick and treat","romance","trickandtreat.jpg");
 insert into books values(7,"charlainharris","horror","charlainharris.jpg");

select *from books;
