<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="com.assignment.books.dbresource.DbConnection"%>
<%@page import="com.assignment.books.dao.BookDao"%>
<%@page import="com.assignment.books.bean.*"%>
<%@page import="java.util.*"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>


<!DOCTYPE html>

<html>

<head>


<meta charset="ISO-8859-1">

<title>Home Page</title>

 <link href="https://fonts.googleapis.com/css?family=ZCOOL+XiaoWei" rel="stylesheet">
 <link href="css/style.css" rel="stylesheet" type="text/css"/>

</head>

<body style="text-align:center;" bgcolor="pink">
<h1>WelCome To BookSore</h1>
<br>
<a href="login.jsp"><button>Login</button></a>
<a href="registration.jsp"><button>Signin</button></a>
<br>
<br>
	<div class="container">
		<div class="card-header my-3">All Available Books Without Login</div>
		<div class="row">
		
		<style>
		.navya{
		float:left;
		background-color: skyblue;
		height:450px;
  		width: 200px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
		}
		.vinay{
		float:left;
		background-color: skyblue;
		height:450px;
  		width: 200px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
		}
		.vasu{
		float:left;
		background-color: skyblue;
		height:450px;
  		width: 200px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
		}
		.arjun{
		float:left;
		background-color: skyblue;
		height:450px;
  		width: 200px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
		}
		.ram{
		float:left;
		background-color: skyblue;
		height:450px;
  		width: 200px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
		}
		.vishnu{
		float:left;
		background-color: skyblue;
		height:450px;
  		width: 200px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
		}
		.sai{
		float:left;
		background-color: skyblue;
		height:450px;
  		width: 200px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
		}
</style>
		
		<div class="navya">
		<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="Book-img-top" src="image/bhagavdgita.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:1</h5>
						<h5 class="booktitle" >BookTitle:"bhagavdgita"</h5>
						<h6 class="bookgenre">Bookgenre:"Motivational"</h6>
						
						<div class="mt-3 d-flex justify-content-between">
							<a class="btn btn-dark"><button>Add to Cart</button></a> 
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="vinay">
		
			<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="Book-img-top" src="image/tomandjerry.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:2</h5>
						<h5 class="booktitle" >BookTitle:"tom and jerry"</h5>
						<h6 class="bookgenre">Bookgenre:"funny"</h6>
						
						<div class="mt-3 d-flex justify-content-between">
							<a class="btn btn-dark"><button>Add to Cart</button></a> 
						</div>
					</div>
				</div>	
			</div>	
		</div>	
			
			<div class="vasu">
			<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="Book-img-top" src="image/lifeamazingsecrets.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:3</h5>
						<h5 class="booktitle" >BookTitle:"life amazing secrets"</h5>
						<h6 class="bookgenre">Bookgenre:"motivational"</h6>
						
						<div class="mt-3 d-flex justify-content-between">
							<a class="btn btn-dark"><button>Add to Cart</button></a> 
						</div>
					</div>
				</div>
			</div>
		</div>
			<div class="arjun">
			<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="Book-img-top" src="image/kidsbooks.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:4</h5>
						<h5 class="booktitle" >BookTitle:"kidsbook"</h5>
						<h6 class="bookgenre">Bookgenre:"funny"</h6>
						
						<div class="mt-3 d-flex justify-content-between">
							<a class="btn btn-dark"><button>Add to Cart</button></a> 
						</div>
					</div>
				</div>
			</div>
		</div>
			<div class="ram">
			<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="Book-img-top" src="image/freeypurmind.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:5</h5>
						<h5 class="booktitle" >BookTitle:" feeyourmind"</h5>
						<h6 class="bookgenre">Bookgenre:"motivationl"</h6>
						
						<div class="mt-3 d-flex justify-content-between">
							<a class="btn btn-dark" href="add-to-favourite?id=2"><button>Add to Cart</button></a> 
						</div>
					</div>
				</div>
			</div>
		</div>
			<div class="vishnu">
			<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="Book-img-top" src="image/trickandtreat.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:6</h5>
						<h5 class="booktitle" >BookTitle:"trick and treat"</h5>
						<h6 class="bookgenre">Bookgenre:"romance"</h6>
						
						<div class="mt-3 d-flex justify-content-between">
							<a class="btn btn-dark" href="add-to-favourite?id=2"><button>Add to Cart</button></a> 
						</div>
					</div>
				</div>
			</div>
		</div>
			
		</div>
	</div>
	<div class="sai">
			<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="Book-img-top" src="image/charlainharris.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:7</h5>
						<h5 class="booktitle" >BookTitle:"charlainharris"</h5>
						<h6 class="bookgenre">Bookgenre:"horror"</h6>
						
						<div class="mt-3 d-flex justify-content-between">
							<a class="btn btn-dark" href="add-to-favourite?id=2"><button>Add to Cart</button></a> 
						</div>
					</div>
				</div>
			</div>
		</div>
			
		</div>
	</div>

</body>
</html>