package com.test;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

import com.books.bean.Books;
import com.books.bean.User;
import com.books.controller.UserController;

class UserControllerTest {
	@Autowired
	UserController userController;

	@Test
	void testGetAllUsersInfo() {
		//fail("Not yet implemented");
		RestTemplate restTemplate=new RestTemplate();
		User user=restTemplate.getForObject("findById/1",User.class);
		assertEquals("Deepika",user.getUsername());
		assertEquals("123",user.getPassword());
		assertEquals(1,user.getId());

	}
	@Test
	void testStoreUserInfo() {
		//fail("Not yet implemented");
		RestTemplate restTemplate=new RestTemplate();
		User user=new User();
		user.setId(2);;
		user.setUsername("Deepika");
		user.setPassword("123");
		String res=restTemplate.postForObject("/storeUser",user,String.class);
		assertEquals(res,"user id must be unique");
	}


}
