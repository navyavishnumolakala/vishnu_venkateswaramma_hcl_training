package com.books.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.books.bean.User;


public interface UserDao extends JpaRepository<User,Integer>{

}
