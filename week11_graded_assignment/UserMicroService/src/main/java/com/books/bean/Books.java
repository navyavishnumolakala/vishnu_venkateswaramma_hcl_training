package com.books.bean;
import javax.persistence.Entity;
import javax.persistence.Id;
@Entity

public class Books {

	@Id
	private int bid;
	private String bname;
	private float price;
	
	public Books() {
		super();
	}
	
	public Books(int bid, String bname, float price) {
		super();
		this.bid = bid;
		this.bname = bname;
		this.price = price;
	}

	public int getBid() {
		return bid;
	}
	public void setBid(int bid) {
		this.bid = bid;
	}
	public String getBname() {
		return bname;
	}
	public void setBname(String bname) {
		this.bname = bname;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Books [bid=" + bid + ", bname=" + bname + ", price=" + price + "]";
	}
	
}

