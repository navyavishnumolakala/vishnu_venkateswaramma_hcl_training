package com.books.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.books.bean.User;
import com.books.service.UserService;

@RestController
@RequestMapping("/User")

public class UserController {
	@Autowired
	UserService userService;
	@GetMapping(value="getAllUser",produces=MediaType.APPLICATION_JSON_VALUE)
	public List<User>getAllUsersInfo(){
		return userService.getAllUsers();
	}
	@PostMapping(value="storeUser",consumes=MediaType.APPLICATION_JSON_VALUE)
	public String storeUserInfo(@RequestBody User user) {
		return userService.storeUserInfo(user);
	}
    @PatchMapping(value="updateUser")
    public String updateUserInfo(@RequestBody User user) {
    	return userService.updateUserInfo(user);
    }
    @DeleteMapping(value="deleteUser/{uid}")
    public String DeleteUserInfo(@PathVariable("uid") int uid) {
    	return userService.DeleteUserInfo(uid);
    }

}
