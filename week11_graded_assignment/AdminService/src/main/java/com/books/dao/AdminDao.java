package com.books.dao;
import org.springframework.data.jpa.repository.JpaRepository;

import com.books.bean.Admin;

public interface AdminDao extends JpaRepository<Admin,Integer>{

}
