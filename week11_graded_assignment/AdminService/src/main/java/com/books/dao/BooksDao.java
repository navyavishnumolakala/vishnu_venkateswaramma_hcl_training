package com.books.dao;
import org.springframework.data.jpa.repository.JpaRepository;

import com.books.bean.Books;


public interface BooksDao extends  JpaRepository<Books,Integer>{

}
