package com.books.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.books.bean.Admin;
import com.books.bean.Books;
import com.books.dao.AdminDao;

@Service

public class AdminService {
	@Autowired
	AdminDao adminDao;
	public String storeRegisterInfo(Admin admin) {
		if(adminDao.existsById(admin.getId())){
			return "u have already registred go to login";
		}else {
			adminDao.save(admin);
			return "register stored successfully";
		}
	}
	public String userloginInfo(Admin admin) {
		if(!adminDao.existsById(admin.getId())) {
			return "user info is not present,plz register";
		}else {
			Admin a=adminDao.getById(admin.getId());
			a.setUsername(a.getUsername());
			a.setPassword(a.getPassword());
			adminDao.save(a);
			return "user login successfully";
		}
	}	

}
