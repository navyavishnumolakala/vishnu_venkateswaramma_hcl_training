package com.books.service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.books.bean.Books;
import com.books.dao.BooksDao;

@Service

public class BooksService {
	@Autowired
	BooksDao booksDao;
	public List<Books>getAllBooks(){
		return booksDao.findAll();
	}
	public String storeBooksInfo(Books book) {
		if(booksDao.existsById(book.getBid())) {
			return "Book Id must be unique";
		}else {
			booksDao.save(book);
			return "Book stored successfully";
		}
	}
	public String updateBooksInfo(Books book) {
		if(! booksDao.existsById(book.getBid())) {
			return "Book info is not present";
		}else {
			Books b=booksDao.getById(book.getBid());
			b.setPrice(book.getPrice());
			booksDao.saveAndFlush(b);
			return "Book Details updated successfully";
		}
	}
	public String DeleteBooksInfo(int bid) {
		if(!booksDao.existsById(bid)) {
			return "Books info is not present";
		}else {
			booksDao.deleteById(bid);
			return "Book Deleted successfully";
		}
	}

}
