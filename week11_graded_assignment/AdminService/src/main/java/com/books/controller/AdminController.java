package com.books.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.books.bean.Admin;
import com.books.service.AdminService;

@RestController
@RequestMapping("/admin")

public class AdminController {
	@Autowired
	AdminService adminService;
	@PostMapping(value="signup",consumes=MediaType.APPLICATION_JSON_VALUE)
   public String storeRegisterInfo(@RequestBody Admin admin) {
		return adminService.storeRegisterInfo(admin);
	}
	@PostMapping(value="signin",consumes=MediaType.APPLICATION_JSON_VALUE)
	public String userloginInfo(@RequestBody Admin admin) {
	return adminService.userloginInfo(admin);
}
	@GetMapping(value="logout")
	public String logoutInfo() {
		return "logout successfully";
	}

}
