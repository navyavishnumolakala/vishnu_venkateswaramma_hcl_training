package com.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

import com.books.bean.Books;
import com.books.controller.BooksController;

class BooksControllerTest {
	@Autowired
	BooksController booksController;

	@Test
	void testGetAllBooksInfo() {
		//fail("Not yet implemented");
		RestTemplate restTemplate=new RestTemplate();
		Books books=restTemplate.getForObject("findById/1",Books.class);
		assertEquals("Every one has a story",books.getBname());
		assertEquals(200,books.getPrice());
		assertEquals(1,books.getBid());

		
	}
	@Test
	void testStoreBooksInfo() {
		//fail("Not yet implemented");
		RestTemplate restTemplate=new RestTemplate();
		Books books=new Books();
		books.setBid(2);
		books.setBname("this is not u r story");
		books.setPrice(300);
		String res=restTemplate.postForObject("/storeBooks",books,String.class);
		assertEquals(res,"Book id must be unique");
	}
	

	@Test
	void testUpdateBooksInfo() {
		//fail("Not yet implemented");
		RestTemplate restTemplate=new RestTemplate();
		Books books=restTemplate.getForObject("updateBooks",Books.class);
		Books books1=new Books();
		books.getBid();
		books.setPrice(400);
		assertEquals(books1,"Book id must be unique");
}}



