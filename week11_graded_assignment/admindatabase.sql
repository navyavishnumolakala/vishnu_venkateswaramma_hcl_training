create database vishnu_week11;
use vishnu_week11;

create table admin(
email_id varchar(100) primary key,
admin_password varchar(100)
);

create table books(
book_id int primary key,
book_name varchar(100),
author varchar(100),
book_genre varchar(100),
book_Image_Url varchar(1000),
book_price float,
book_rating float
);

insert into books
value(1,"The Bagvadgeetha","sinchan","Motivational","https://www.learnreligions.com/thmb/vYATNacaguU3722EFQWN__agg7o=/650x0/filters:no_upscale():max_bytes(150000):strip_icc():format(webp)/asitis-56a483335f9b58b7d0d758e9.JPG",
950, 5.0 );


insert into books
value(2,"My Life","A.P.J.AbdhulKalam","AutoBiogrhaphy","https://m.media-amazon.com/images/I/51AGCruw10L._SY346_.jpg",
1150, 4.0 );

insert into books
value(3,"Crmilla","Jhon","Horror","https://www.bing.com/th?id=AMMS_5d1a51956cf72d35bb991115eaf83a75&w=124&h=168&c=7&o=6&pid=SANGAM",
1150, 4.0 );

select * from books;

select * from admin;

