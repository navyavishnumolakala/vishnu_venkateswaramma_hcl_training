<%@page import="com.bean.UserOrder"%>
<%@page import="java.util.*" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<div align="center">
<table border="1">
	<tr>
	        <th>SNO</th>
			<th>ITEMId</th>
			<th>ITEMNAME</th>
			<th>ITEMTYPE</th>
			<th>ITEMPRICE</th>
			<th>User</th>
			
			
	</tr>
<%  
	
    Object obj = session.getAttribute("uo");
	List<UserOrder> listOfItems = (List<UserOrder>)obj;
	Iterator<UserOrder> ii = listOfItems.iterator();
	while(ii.hasNext()){
		UserOrder uo  = ii.next();
		%>
		<tr>
		    <td><%=uo.getSno() %></td>
			<td><%=uo.getItemId() %></td>
			<td><%=uo.getItemName()%></td>
			<td><%=uo.getItemType() %></td>
			<td><%=uo.getItemPrice() %></td>
			<td><%=uo.getName() %></td>
		</tr>
		<% 
	}
%>

</table>
</div>
<a href="todaysum">Click Here To View TotalBill Generated Today</a>

</body>
</html>