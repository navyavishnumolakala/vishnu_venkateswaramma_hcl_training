create database vishnu_mini

use vishnu_mini;

create table admin(email varchar(255),password varchar(255));

insert into admin values('admin@gmail.com',12345);


create table users(email varchar(255),password varchar(255));


insert into users values('navya@gmail.com','Navya123@');

insert into users values('vishnu@gmail.com','Navya123@');


create table items(item_id int primary key, item_name varchar(255),item_price float, item_type varchar(255));

insert into items values(1,'dosa',50,'1_plate');
 insert into items values(2,'chapathi',100,'1_plate');
 insert into items values(3,'Chiken_Biryani',200,'1_plate');
insert into items values(4,'Chiken_curry',150,'1_plate');
insert into items values(5,'Chiken_65',250,'1_plate');
insert into items values(6,'Butter_chicken',550,'1_plate');
insert into items values(7,'Chicken_noodel_soup',100,'1_plate');
insert into items values(8,'Japaness dish',450,'1_plate');
insert into items values(9,'Tandoori_chiken',550,'1_plate');
insert into items values(10,'chicken_roosters',250,'1_plate');
insert into items values(11,'tea',30,'1_single');
 insert into items values(12,'Coffie',100,'1_single');
insert into items values(13,'Boost',60,'1_single');
 insert into items values(14,'Ice_Cream',100,'1_single');
insert into items values(15,'vanilla',120,'1_single');
insert into items values(16,'Chocolate',130,'1_single');


create table user_order(sno int primary key, item_id int, item_name varchar(255),item_type varchar(255),item_price float,name varchar(255));

insert into user_order values(100,1,'chiken_biryani','1_plate',200,'navya');

