package com.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.Admin;
import com.dao.AdminDao;

@Service
public class AdminService {
	@Autowired
	AdminDao adminDao;
	
   public String adminRegistration(Admin adm) {
	   if(adminDao.existsById(adm.getEmail())) {
		   return "your Details Already Present";
	   }else {
		   adminDao.save(adm);
		   return "Details Saved Sucessfully";
	   }	   
   }
   
   public String login(Admin adm) {
	   if(adminDao.existsById(adm.getEmail())) {
		   Admin a=adminDao.getById(adm.getEmail());
		   if(a.getPassword().equals(adm.getPassword())) {
			   return "You Logged In sucessfully";
		   }else {
			   return "please enter valid details";
		   }				   
	   }else {
		   return "Your Details Are Not Present";
	   }
	   
   }
   

}
