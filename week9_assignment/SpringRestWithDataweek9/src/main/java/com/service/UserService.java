package com.service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.User;
import com.dao.UserDao;

@Service
public class UserService {
	@Autowired
	UserDao userDao;
	
	public List<User> getAllUsers() {
		return userDao.findAll();
	}
	
	public String storeUserInfo(User use) {
				
						if(userDao.existsById(use.getUid())) {
									return "User id must be unique";
						}else {
									userDao.save(use);
									return "username stored successfully";
						}
	}
	
	public String deleteUserInfo(int uid) {
		if(!userDao.existsById(uid)) {
			return "username not present";
			}else {
			userDao.deleteById(uid);
			return "username deleted successfully";
			}	
	}
	
	public String updateUserInfo(User use) {
		if(!userDao.existsById(use.getUid())) {
			return "username not present";
			}else {
			User u	= userDao.getById(use.getUid()); 
			u.setUsername(use.getUsername());	
			userDao.saveAndFlush(u);			
			return "username updated successfully";
			}	
	}
	
}


