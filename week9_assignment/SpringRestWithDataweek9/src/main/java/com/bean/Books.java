package com.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Setter					// all setter methods. 
@Getter					// all getter methods 
@ToString				// toString() method 
@NoArgsConstructor			// empty constructor 
@RequiredArgsConstructor			// parameterized constructor 
public class Books{
@Id
private int bookid;
private String bname;
private float price;
public int getBookid() {
	return bookid;
}
public void setbookid(int bookid) {
	this.bookid = bookid;
}
public String getBname() {
	return bname;
}
public void setBname(String bname) {
	this.bname = bname;
}
public float getPrice() {
	return price;
}
public void setPrice(float price) {
	this.price = price;
}

}
