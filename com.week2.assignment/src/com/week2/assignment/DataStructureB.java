package com.week2.assignment;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
public class DataStructureB {
	 public void cityNameCount(ArrayList<Employee> employees) {
		 System.out.println("Count Of Employees From Each City");
         Set<String> l = new TreeSet<String>();

         ArrayList<String> a = new ArrayList<String>();
        
         for (Employee s : employees) {

                    a.add(s.getCity());

                    l.add(s.getCity());
         }

         for (String s : l) {

                    System.out.print(s + "=" + Collections.frequency(a, s) + " ");
         }
}

public void monthlySalary(ArrayList<Employee> employees) {
	 System.out.println("Monthly Salary Of Employee Along With Their ID Is:");
         for (Employee e : employees) {
      //System.out.print("Monthly Salary Of Employee Along With Their ID Is");
                    System.out.print(e.getId() + "=" + e.getSalary() / 12 + " ");

         }

}
}
